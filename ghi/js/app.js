// window.addEventListener('DOMContentLoaded', async () => {

//     const url = 'http://localhost:8000/api/conferences/';

//     try{
//         const response = await fetch(url);

//         if (!response.ok) {
//             console.log("Improper response")
//         } else {
//         const data = await response.json();

//         const conference = data.conferences[0];

//         const nameTag = document.querySelector('.card-title');
//         nameTag.innerHTML = conference.name;


//         const detailUrl = `http://localhost:8000${conference.href}`;
//         console.log(detailUrl);
//         const detailResponse = await fetch (detailUrl);
//         if (detailResponse.ok) {
//         //     const details = await detailResponse.json();

//         //     const description = details.conference.description;
//         //     const descriptionTag = document.querySelector('.card-text');
//         //     descriptionTag.innerHTML = description;
//         //     console.log(details)

//         //     const image = details.conference.location.picture_url;
//         //     const imageTag = document.querySelector('.card-img-top');
//         //     imageTag.src = image;
//         for (let conference of data.conferences) {
//             const detailUrl = `http://localhost:8000${conference.href}`;
//             const detailResponse = await fetch(detailUrl);
//             if (detailResponse.ok) {
//                 const details = await detailResponse.json();
//                 const title = details.conference.title;
//                 const description = details.conference.description;
//                 const pictureUrl = details.conference.location.picture_url;
//                 const html = createCard(title, description, pictureUrl);
//                 const column = document.querySelector('.col');
//                 column.innerHTML += html;
//             }
//         }

//         }
//         }
//     } catch (e) {
//         console.log("silly goose.")
//     }
// });




//   <div class="card">
//             <img src="..." class="card-img-top" alt="...">
//             <div class="card-body">
//               <h5 class="card-title">Conference name</h5>
//               <p class="card-text">Conference description</p>
//             </div>
//           </div>

function createCard(name, description, pictureUrl, start, end, locale) {
    return `
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <d6 class="card-subtitle mb-2 text-muted">${locale}<d6>
          <div>_____________________</div>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">${start} - ${end}</div>

      </div>
    `;
  }

  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        console.log("Improper response")
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const startDate = new Date(details.conference.starts).toLocaleDateString();
            const endDate = new Date(details.conference.ends).toLocaleDateString();


            const locale = details.conference.location.name;



            //console.log("!!!!!!", testDate)
            const html = createCard(title, description, pictureUrl, startDate, endDate, locale);
            const column = document.querySelector('.col');
            column.innerHTML += html;
            console.log(html)
          }
        }

      }
    } catch (e) {
        console.log("silly goose.")
    }

  });
