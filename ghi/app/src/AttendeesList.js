function AttendeesList(props){
    console.log(props)
    if (props.attendees === undefined) {
        console.log("attendees lost at attendees list")
        return null
    }

        return (
        <div className="container">
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Conference</th>
          </tr>
        </thead>
        <tbody>
        {props.attendees.map(attendee => {
  return (
    <tr key={attendee.href}>
      <td>{ attendee.name }</td>
      <td>{ attendee.conference }</td>
    </tr>
  );
})}
        </tbody>
      </table>
    </div>

)}

export default AttendeesList;

// function AttendeesList(props) {
//   console.log("attList:", props.attendees);
//   return (
//     <table className="table table-striped">
//       <thead>
//         <tr>
//           <th>Name</th>
//           <th>Conference</th>
//         </tr>
//       </thead>
//       <tbody>
//         {props.attendees.map(attendee => {
//           return (
//             <tr key={attendee.href}>
//               <td>{ attendee.name }</td>
//               <td>{ attendee.conference }</td>
//             </tr>
//           );
//         })}
//       </tbody>
//     </table>
//   );
// }

// export default AttendeesList;
