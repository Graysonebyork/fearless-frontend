import React, { useEffect, useState } from 'react';

function PresentationForm(){

  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [companyName, setCompanyName] = useState('');
  const [title, setTitle] = useState('');
  const [synopsis, setSynopsis] = useState('');
  const [conference, setConference] = useState('');
  const [conferences, setConferences] = useState([]);


  const fetchData = async () => {
    const url = 'http://localhost:8000/api/conferences/'
    const response = await fetch(url)
    console.log("fetch successful")
    if (response.ok){
      const data = await response.json();
      console.log("!!!!!presentaion data:", data)
      setConferences(data.conferences);
    }

  }

  useEffect(() => {
    fetchData();
  }, []);

  const handleChangeName = (event) => {
    const value = event.target.value;
    setName(value);
  }
  const handleChangeEmail = (event) => {
    const value = event.target.value;
    setEmail(value);
  }
  const handleChangeCompanyName = (event) => {
    const value = event.target.value;
    setCompanyName(value);
  }
  const handleChangeTitle = (event) => {
    const value = event.target.value;
    setTitle(value);
  }
  const handleChangeSynopsis = (event) => {
  const value = event.target.value;
  setSynopsis(value);
  }
  const handleChangeConference = (event) => {
    const value = event.target.value;
    setConference(value);
  }


const handleSubmit = async (event) => {
  event.preventDefault();
  const data = {};
  data.presenter_name = name;
  data.presenter_email = email;
  data.title = title;
  data.company_name = companyName;
  data.synopsis = synopsis;
  data.conference = conference;
  // data.status = "SUBMITTED";
  console.log("this is the data I made:", data);

  const formTag = document.getElementById('create-presentation-form');
  const selectTag = document.getElementById('conference');

  const conferenceId = selectTag.options[selectTag.selectedIndex].value;

  const presentationUrl = `http://localhost:8000${conferenceId}presentations/`

  const fetchOptions = {
    method: 'post',
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json',
    },
  }
  const presentationResponse = await fetch(presentationUrl, fetchOptions);
  if (presentationResponse.ok) {
  const newPresentation = await presentationResponse.json();
  console.log(newPresentation);

    setCompanyName('');
    setName('');
    setEmail('');
    setTitle('');
    setSynopsis('');
    setConference('');
  }
}


// console.log("presentation data:", data)
// console.log("presentation data:", data.conferences)

return (

  <>
  <div className="container">
  <div className="row">
    <div className="offset-3 col-6">
      <div className="shadow p-4 mt-4">
        <h1>Create a new presentation</h1>
        <form onSubmit={handleSubmit} id="create-presentation-form">
          <div className="form-floating mb-3">
            <input onChange={handleChangeName} required placeholder="Presenter name" value={name} required type="text" name="presenter_name" id="presenter_name" className="form-control"/>
            <label htmlFor="presenter_name">Presenter name</label>
          </div>
          <div className="form-floating mb-3">
            <input onChange={handleChangeEmail} placeholder="Presenter email" value={email} required type="email" name="presenter_email" id="presenter_email" className="form-control"/>
            <label htmlFor="presenter_email">Presenter email</label>
          </div>
          <div className="form-floating mb-3">
            <input onChange={handleChangeCompanyName} placeholder="Company name" value={companyName} type="text" name="company_name" id="company_name" className="form-control"/>
            <label htmlFor="company_name">Company name</label>
          </div>
          <div className="form-floating mb-3">
            <input onChange={handleChangeTitle} placeholder="Title" value={title} required type="text" name="title" id="title" className="form-control"/>
            <label htmlFor="title">Title</label>
          </div>
          <div className="mb-3">
            <label htmlFor="synopsis">Synopsis</label>
            <textarea onChange={handleChangeSynopsis} className="form-control" value={synopsis} id="synopsis" rows="3" name="synopsis" ></textarea>
          </div>
          <div className="mb-3">
            <select onChange={handleChangeConference} required name="conference" value={conference} id="conference" className="form-select">
              <option value="">Choose a conference</option>
              {conferences.map(conference => {
                return (
                  <option key={conference.href} value={conference.href}>{conference.name}</option>
                )
              })}

            </select>
          </div>
          <button className="btn btn-primary">Create</button>
        </form>
      </div>
    </div>
  </div>
</div>
</>


);
}





export default PresentationForm;
