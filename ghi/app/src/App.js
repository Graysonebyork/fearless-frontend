import Nav from './Nav';
import MainPage from './MainPage';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import AttendConferenceForm from './AttendConferenceForm'
import ConferenceForm from './ConferenceForm'
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import PresentationForm from './PresentationForm';

function App(props) {
  console.log("app props:", props.attendees)
  if (props.attendees === undefined) {
    return null
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className='container'>
        <Routes>
          {/* <Route index element={<MainPage />} /> */}
          <Route path="/" element={<MainPage/>}/>
          <Route path="locations/new" element={<LocationForm/>}/>
          <Route path="conferences/new" element={<ConferenceForm/>}/>
          <Route path="attendees/new" element={<AttendConferenceForm/>}/>
          <Route path="attendees" index element={<AttendeesList attendees={props.attendees}/>}/>
          <Route path="presentations/new" element={<PresentationForm />}/>
        </Routes>
      {/* <AttendeesList attendees={props.attendees}/> */}
      {/* <LocationForm /> */}
      </div>
    </ BrowserRouter>
  );
}

export default App;
